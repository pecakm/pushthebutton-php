<?php
    session_start();

    // Move to index.php if credentials are not set
    if (!isset($_POST['login']) || !isset($_POST['password'])) {
        header('Location: index.php');
        exit();
    }

    require_once "connect.php";

    try {
        $connection = new mysqli($host, $db_user, $db_password, $db_name);
        // If there is no errors
        if ($connection->connect_errno == 0) {
            $login = $_POST['login'];
            $login = htmlentities($login, ENT_QUOTES, "UTF-8");
            $password = $_POST['password'];
            
            if ($answer = $connection->query(
                sprintf("select * from users where nickname='%s'",
                    mysqli_real_escape_string($connection, $login))
            )) {
                // If there is any result from database
                if ($answer->num_rows > 0) {
                    $row = $answer->fetch_assoc();

                    // If password is correct
                    if (password_verify($password, $row['password'])) {
                        $_SESSION['nickname'] = $row['nickname'];
                        $_SESSION['overall'] = $row['overall'];
                        $_SESSION['atonetime'] = $row['atonetime'];
                        $answer->free_result();
                        header('Location: index.php');
                    } else {
                        header('Location: signin.php');
                    }
                } else {
                    header('Location: signin.php');
                }
            } else {
                throw new Exception($connection->error);
            }

            $connection->close();
        } else {
            throw new Exception($connection->error);
        }
    } catch (Exception $exception) {
        echo 'Something went wrong. Sorry! :(';
        exit();
    }
?>