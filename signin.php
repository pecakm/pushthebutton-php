<?php
    session_start();

    // If logged in move to index.php
    if (isset($_SESSION['nickname'])) {
        header("Location: index.php");
        exit();
    }
?>

<div id="loginForm">
    <form action="login.php" method="post">
        Nickname:<br>
        <input type="text" name="login"><br><br>
        Password:<br>
        <input type="password" name="password"><br><br>
        <input type="submit" value="Sign In">
    </form>
    <br><br><br>
    Don't want to sign in? <a href="#" class="main">Go back to practice.</a>
</div>


