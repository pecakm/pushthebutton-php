<?php
    session_start();

    if (!isset($_SESSION['nickname'])) {
        header('Location: signin.php');
        exit();
    }

    require_once "connect.php";

    try {
        $connection = new mysqli($host, $db_user, $db_password, $db_name);
        if ($connection->connect_errno == 0) {
            $counter = $_POST['counter'];
            if ($counter > $_SESSION['atonetime']) {
                $_SESSION['atonetime'] = $counter;
            }
            $atonetime = $_SESSION['atonetime'];
            $nickname = $_SESSION['nickname'];
            $sql = "update users set overall = overall + '$counter', atonetime = '$atonetime' where nickname='$nickname'";
            if ($answer = $connection->query($sql)) {
                $_SESSION['overall'] = $_SESSION['overall'] + $counter;
                header('Location: index.php');
            } else {
                throw new Exception($connection->error);
            }
    
            $connection->close();
        } else {
            throw new Exception($connection->error);
        }
    } catch (Exception $exception) {
        echo 'Something went wrong. Sorry! :(';
        exit();
    }
?>