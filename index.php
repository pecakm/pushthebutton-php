<?php
    session_start();

    require_once "connect.php";

    try {
        $connection = new mysqli($host, $db_user, $db_password, $db_name);
        
        // Fill the TOP3 tables
        if ($connection->connect_errno == 0) {
            $sql = "select nickname, overall from users order by overall desc";
            if ($answer = $connection->query($sql)) {
                $bestPlayers[0] = "";
                $bestPlayers[1] = "";
                $bestPlayers[2] = "";
                $bestScores[0] = "";
                $bestScores[1] = "";
                $bestScores[2] = "";
                if ($answer->num_rows > 0) {
                    $index = 0;
                    while ($row = $answer->fetch_assoc()) {
                        $bestPlayers[$index] = $row['nickname'];
                        $bestScores[$index] = $row['overall'];
                        $index = $index + 1;
                    }
                    $answer->free_result();
                }
            } else {
                throw new Exception($connection->error);
            }

            $sql = "select nickname, atonetime from users order by atonetime desc";
            if ($answer = $connection->query($sql)) {
                $AOTbestPlayers[0] = "";
                $AOTbestPlayers[1] = "";
                $AOTbestPlayers[2] = "";
                $AOTbestScores[0] = "";
                $AOTbestScores[1] = "";
                $AOTbestScores[2] = "";
                if ($answer->num_rows > 0) {
                    $index = 0;
                    while ($row = $answer->fetch_assoc()) {
                        $AOTbestPlayers[$index] = $row['nickname'];
                        $AOTbestScores[$index] = $row['atonetime'];
                        $index = $index + 1;
                    }
                    $answer->free_result();
                }
            } else {
                throw new Exception($connection->error);
            }

            $connection->close();
        } else {
            throw new Exception($connection->error);
        }
    } catch (Exception $exception) {
        echo 'Something went wrong. Sorry! :(';
        exit();
    }
?>

<!doctype html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Push The Button - Game by PecakM</title>
    <meta name="description" content="Push The Button Game. Just play it!">
    <meta name="keywords" content="play, button, game, rank, score, fun, bored">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="container">
        <header>
            <div id="logo">
                <a href="index.php">
                    PUSH<br>
                    THE<br>
                    BUTTON
                </a>
            </div>

<?php
    if (isset($_SESSION['nickname'])) {
        echo '<div id="header">Hello, '.$_SESSION['nickname'].'! <a href="logout.php">Logout</a></div>';
    } else {
        echo '<div id="header"><a href="#" class="signin">Sign in</a> or <a href="#" class="signup">sign up</a> to save your results!</div>';
    }
?>
            <div style="clear: both;"></div>
        </header>
        <aside id="sidebar">
            <div class="results">
                TOP3 overall:<br>
                <table>
                    <tr class="firstPlace">
<?php
    echo '<td>1.</td><td>'.$bestPlayers[0].'</td><td>'.$bestScores[0].'</td>';
?>
                    </tr>
                    <tr class="secondPlace">
<?php
    echo '<td>2.</td><td>'.$bestPlayers[1].'</td><td>'.$bestScores[1].'</td>';
?>
                    </tr>
                    <tr class="thirdPlace">
<?php
    echo '<td>3.</td><td>'.$bestPlayers[2].'</td><td>'.$bestScores[2].'</td>';
?>
                    </tr>
                </table>
<?php
    if (isset($_SESSION['nickname'])) {
        echo 'Your result: '.$_SESSION['overall'];
    } else {
        echo '<br>';
    }
?>
            </div>

            <div class="results">
                TOP3 at one time:<br>
                <table>
                <tr class="firstPlace">
<?php
    echo '<td>1.</td><td>'.$AOTbestPlayers[0].'</td><td>'.$AOTbestScores[0].'</td>';
?>
                    </tr>
                    <tr class="secondPlace">
<?php
    echo '<td>2.</td><td>'.$AOTbestPlayers[1].'</td><td>'.$AOTbestScores[1].'</td>';
?>
                    </tr>
                    <tr class="thirdPlace">
<?php
    echo '<td>3.</td><td>'.$AOTbestPlayers[2].'</td><td>'.$AOTbestScores[2].'</td>';
?>
                    </tr>
                </table>
<?php
    if (isset($_SESSION['nickname'])) {
        echo 'Your result: '.$_SESSION['atonetime'];
    } else {
        echo '<br>';
    }
?>
            </div>
        </aside>
        <main>
            <article id="content"></article>
        </main>
        <div style="clear: both;"></div>
        <footer id="footer">
            Copyright &copy; 2018 PecakM
        </footer>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="push.js" type="text/javascript"></script>
    <script src="ajax.js" type="text/javascript"></script>
</body>
</html>