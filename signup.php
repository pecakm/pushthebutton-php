<?php
    session_start();

    // If logged in move to index.php
    if (isset($_SESSION['nickname'])) {
        header("Location: index.php");
        exit();
    }

    require_once "connect.php";
    mysqli_report(MYSQLI_REPORT_STRICT);

    // Credentials validation
    if (isset($_POST['nickname'])) {
        $validCredentials = true;
        $nickname = $_POST['nickname'];

        if (strlen($nickname) < 3 || strlen($nickname) > 20) {
            $validCredentials = false;
        }

        if (!ctype_alnum($nickname)) {
            $validCredentials = false;
        }

        $password1 = $_POST['password1'];
        $password2 = $_POST['password2'];

        if (strlen($password1) < 3 || strlen($password1) > 20) {
            $validCredentials = false;
        }

        if ($password1 != $password2) {
            $validCredentials = false;
        }

        $passwordHash = password_hash($password1, PASSWORD_DEFAULT);

        $captcha_secret_key = "6Le5AkgUAAAAACvxtDtJKBB4WdnaMNbDZeODOZ_Z";
        $check_captcha = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$captcha_secret_key.'&response='.$_POST['g-recaptcha-response']);
        $response = json_decode($check_captcha);
        if (!($response->success)) {
            $validCredentials = false;
        }

        try {
            $connection = new mysqli($host, $db_user, $db_password, $db_name);
            if ($connection->connect_errno == 0) {
                $sql = "select id from users where nickname='$nickname'";
                $answer = $connection->query($sql);
                if (!$answer) {
                    throw new Exception($connection->error);
                }
                if ($answer->num_rows > 0) {
                    $validCredentials = false;
                }

                if ($validCredentials) {
                    $sql = "insert into users values (null, '$nickname', '$passwordHash', 0, 0)";
                    if ($connection->query($sql)) {
                        header('Location: signin.php');
                    } else {
                        throw new Exception($connection->error);
                    }
                }

                $connection->close();
            } else {
                throw new Exception(mysqli_connect_errno());
            }
        } catch (Exception $exception) {
            echo 'Something went wrong. Sorry! :(';
            exit();
        }
    }
?>

<div id="loginForm">
    <form method="post">
        Nickname:<br>
        <input type="text" name="nickname"><br><br>
        Password:<br>
        <input type="password" name="password1"><br><br>
        Repeat password:<br>
        <input type="password" name="password2"><br><br>
        <div class="g-recaptcha" data-sitekey="6Le5AkgUAAAAAONxDsr3hJyyShvn5WqCvFwLRmYu"></div><br><br>
        <input type="submit" value="Sign Up">
    </form>
    <br><br><br>
    Don't want to sign up? <a href="#" class="main">Go back to practice.</a>
</div>

<script src='https://www.google.com/recaptcha/api.js'></script>
